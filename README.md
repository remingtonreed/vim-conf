# INSTALL #
1. Clone this repository to your .vim folder

    ```
    $ git clone git@bitbucket.org:rreed/vim-conf.git ~/.vim
    ```

2. Link the _vimrc and _gvimrc to your home folder.

    ```
    $ ln -s ~/.vim/_vimrc ~/.vimrc
    ```

    ```
    $ ln -s ~/.vim/_gvimrc ~/.gvimrc
    ```

3. Install Vundle

    ```
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    ```

4. Launch vim and run

    ```
    :PluginInstall
    ```

5. Done, Enjoy!